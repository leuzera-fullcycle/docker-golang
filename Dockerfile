FROM golang:1.21.3 AS builder

WORKDIR /usr/src/app

COPY . .

RUN go build -o ./main main.go


FROM scratch

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/main ./main

CMD [ "./main" ]
